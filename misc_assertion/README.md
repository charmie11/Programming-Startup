# 表明 (assert)
プログラムの前提条件を明示的に記載することを表明(assertion)と呼びます．
例えば，自然対数の値をlog(x)と計算する時はx>0とならなければいけませんし，5個以上の数値が格納された配列vの全要素の平均値を計算する時は配列のサイズが5以上でなければいけません．
このように関数や特定の処理，変数が何かしらの条件を満たすことを前提とする時は表明(assert)という形で明示的に条件を記述することでコードの可読性が上がり，実行時エラー発生時のデバッグを容易にできるといったメリットがあります．

## 課題
C++, Pythonどちらの言語を使っても構わないので，以下の内容を実装してください．
* 今まで書いたプログラムにコメントを追加する．

## C++
C++では，[cassert](http://cpprefjp.github.io/reference/cassert.html)(assert.h)で定義されたマクロ[assert](http://cpprefjp.github.io/reference/cassert/assert.html)が表明の機能を提供します．
*assert(条件文)* と，assert文を書いた場所で満たされるべき条件を指定します．
条件文がTrueであれば何も起きず，Falseであればエラーメッセージがターミナルに表示されプログラムが強制的に終了されます．
```cpp
#include <cassert>
void func(const int x)
{
    assert(x >= 0 && "The input variable x must be non-negative value.");
}

int main()
{
    func(0);
    func(3);
    func(-3); // program fails

    return 0;
}
```
上記のコードを実行すると，以下のようにターミナルに出力されます．<br>
x is 0<br>
x is 3<br>
main_assert: main_assert.cpp:6: void func(int): Assertion \`x >= 0 && "The input variable x must be non-negative value."' failed.<br>
Aborted (core dumped)<br>

## Python
Pythonでは，[assert文](http://docs.python.jp/2/reference/simple_stmts.html#the-assert-statement)が表明の機能を提供します．
*assert 条件文* と，assert文を書いた場所で満たされるべき条件を指定します．
C++のassertマクロと同様，条件文がTrueであれば何も起きず，Falseであればエラーメッセージがターミナルに表示されプログラムが強制的に終了されます．
```python
# for Python 2
def func(x):
    assert x >= 0, "The input variable x must be non-negative value."
    print "x is", x

func(0)
func(3)
func(-3) # program fails
```

```python
# for Python 3
def func(x):
    assert x >= 0, "The input variable x must be non-negative value."
    print("x is", x)

func(0)
func(3)
func(-3) # program fails
```
上記のコードを実行すると，以下のようにターミナルに出力されます．
x is 0<br>
x is 3<br>
Traceback (most recent call last):<br>
<br>
  File "<ipython-input-1-5e67d834ba2a>", line 7, in <module><br>
    func(-3) # program fails<br>
<br>
  File "<ipython-input-1-5e67d834ba2a>", line 2, in func<br>
    assert x >= 0, "The input variable x must be non-negative value."<br>
<br>
AssertionError: The input variable x must be non-negative value.<br>
