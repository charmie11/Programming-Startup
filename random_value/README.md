# 乱数 (Random value)
ここでは乱数について学びます．

## 課題
C++, Pythonどちらの言語を使っても構わないので，以下の内容を実装してください．
1. 正規分布，一様分布以外の分布について調べて，使ってみましょう．
* 大量に生成した乱数からグラフを作成し，本当に分布に従った乱数が生成されているか確認してみてください．

## C++
C++11の標準ライブラリである乱数ライブラリ [*random*](http://cpprefjp.github.io/reference/random.html) を使った乱数生成を学びます．
基本的な流れは
1. 乱数生成器の宣言
* 乱数生成器にシードを与える(初期化)
* 分布生成器の宣言
* 乱数生成
となります．

### 正規分布
正規分布の生成器で最もよく使われるものは *std::normal_distribution* です．
正規分布のパラメータは平均値 *mean* と標準偏差 *stddev > 0* の2つになります．
各パラメータのデフォルト値は *mean = 0, stddev = 1.0* です．
デフォルト値以外の数値に設定する場合，*std::normal_distribution* では，コンストラクタで以下のように指定します．
```cpp
#include <random>

int main()
{
    double mean = 3.5;
    double stddev = 1.2;
    std::random_device seed_gen;
    std::default_random_engine engine(seed_gen());

    // 平均3.5, 標準偏差1.2の正規分布を生成
    std::normal_distribution<> dist(mean, stddev);
    for(int n = 0; n < 10; ++n)
    {
        std::cout << dist(engine) << std::endl;
    }

    return 0;
}
```

### 一様分布
一様分布の生成器は *std::uniform_int_distribution, std::uniform_real_distribution* の2つがあり，それぞれ整数，実数用の乱数生成器です．
一様分布のパラメータは，乱数の下限値 *a* と上限値 *b >= a* の2つになり，乱数生成器はa以上b以下([a, b])の値を生成します．
各パラメータのデフォルト値は *a = 0, b = numeric_limits<IntType>::max()* もしくは *b = numeric_limits<RealType>::max()* となります．
デフォルト値以外の数値に設定する場合，コンストラクタで以下のように指定します．
```cpp
#include <random>

int main()
{
    int a_int = 3;
    int b_int = 6;
    double a_real = 2.7;
    double b_real = 9.1;

    std::random_device seed_gen;
    std::default_random_engine engine(seed_gen());

    // 平均3.5, 標準偏差1.2の正規分布を生成
    std::uniform_int_distribution<> dist_int(a_int, b_int);
    std::uniform_real_distribution<> dist_real(a_real, b_real);
    for(int n = 0; n < 10; ++n)
    {
        std::cout << dist_int(engine) << ", " << dist_real(engine) << std::endl;
    }

    return 0;
}
```

## Python
Pythonの [*random*](http://docs.python.jp/2/library/random.html)，[*numpy.random*](https://docs.scipy.org/doc/numpy/reference/routines.random.html)モジュールを使った乱数生成を学びます．
list型オブジェクトの要素をランダムに選択する，要素の順序をランダムに変更するといった処理であればrandomモジュールを使い，nump.array型オブジェクトに乱数生成した数値を使いたいのであればnumpy.randomモジュールを使うと良さそうです．

### randomモジュール
以下のような関数が提供されています．
* *random.normalvariate(mu, sigma)*: 平均 *mu* ，標準偏差 *sigma* の正規分布に基づく乱数を生成する．
* *random.uniform(a, b)* : a <= N < b もしくは b <= N < aとなる浮動小数点Nを返す
* *random.randint(a, b)* : a <= N <= b となる整数値Nを返す
* *random.randrange(start, stop, step)* : range(start, stop, step)の要素からランダムに選ばれた要素を返す．
* *random.choice(seq)* : 空でないシーケンス *seq* からランダムに要素を返す
* *random.shuffle(x)* : シーケンス *x* をインプレースで混ぜます．
* *random.sample(population, k)* : 母集団のシーケンス *population* の中から *k* 個の要素を選択する．
```python
# for Python 2
import random
print random.random()
print random.uniform(1, 10)
print random.randint(1, 10)
print random.randrange(0, 101, 2)
print random.choice('abcdefghij')
items = [1, 2, 3, 4, 5, 6, 7]
random.shuffle(items)
print items
print random.sample([1, 2, 3, 4, 5],  3)
```

```python
# for Python 3
import random
print(random.random())
print(random.uniform(1, 10))
print(random.randint(1, 10))
print(random.randrange(0, 101, 2))
print(random.choice('abcdefghij'))
items = [1, 2, 3, 4, 5, 6, 7]
random.shuffle(items)
print(items)
print(random.sample([1, 2, 3, 4, 5],  3))
```
上記のコードを実行すると，以下のような結果が得られます．
0.407141305418 <br>
5.40390789298 <br>
8 <br>
86 <br>
a <br>
[2, 6, 3, 7, 4, 1, 5] <br>
[5, 3, 2] <br>
乱数生成のため，皆さんが実行した時の結果と一致する可能性はほぼゼロである点に留意して下さい．

### numpy.random モジュール
numpy.randomモジュールの利点は，一様分布，正規分布から生成した擬似乱数を要素に持つnumpy.array型のオブジェクトを生成できる点です．

#### 正規分布
正規分布の生成器の説明は[ここ](https://docs.scipy.org/doc/numpy/reference/generated/numpy.random.randn.html#numpy.random.randn)に記載されています．
各要素の値が平均0.0，分散1.0の正規分布から生成され，入力引数で指定されたshapeのnumpy.arrayを出力します．
平均mu，標準偏差sigmaの正規分布にしたければ，以下のように書きます．
```python
sigma * np.random.randn(...) + mu
```

#### 正規分布
一様分布の生成器の説明は[ここ](https://docs.scipy.org/doc/numpy/reference/generated/numpy.random.rand.html#numpy.random.rand)に記載されています．
各要素の値が0.0以上，1.0未満となる一様分布から生成され，入力引数で指定されたshapeのnumpy.arrayを出力します．
[a,b)(a以上b未満)の一様分布にしたければ，以下のように書きます．
```python
(b-a) * numpy.random.rand(...) + a
```

#### 要素のシャッフル
numpy.array型オブジェクトの要素の順番をランダムに変更(シャッフル)します．
```python
# for Python 2
arr = np.arange(10)
print 'before shuffle:', arr
np.random.shuffle(arr)
print 'after shuffle:', arr
```

```python
# for Python 3
arr = np.arange(10)
print('before shuffle:', arr)
np.random.shuffle(arr)
print('after shuffle:', arr)
```
上記のコードを実行すると，以下のような出力が得られます．<br>
before shuffle: [0 1 2 3 4 5 6 7 8 9] <br>
after shuffle:  [5 4 7 8 1 6 0 2 9 3]

多次元配列が入力された時は，最初のindexに添ってシャッフルされる点に気をつけて下さい．
```python
# for Python 2
arr = np.arange(12).reshape((3,4))
print 'before shuffle:\n', arr
np.random.shuffle(arr)
print 'after shuffle:\n', arr
```

```python
# for Python 3
arr = np.arange(12).reshape((3,4))
print('before shuffle:\n', arr)
np.random.shuffle(arr)
print('after shuffle:\n', arr)
```
上記のコードを実行すると，arr[i,0], arr[i,1], arr[i,2], i=0, ...,3がシャッフルされることがわかります．<br>
before shuffle: <br>
[[ 0  1  2  3] <br>
 [ 4  5  6  7] <br>
 [ 8  9 10 11]] <br>
after shuffle: <br>
[[ 8  9 10 11] <br>
 [ 0  1  2  3] <br>
 [ 4  5  6  7]]
