# 可視化 (Visualization)
ここではグラフの作成によるデータの可視化に挑戦します．

Pythonでは[Matplotlib](http://matplotlib.org/)という2次元プロット作成用パッケージを使うことが一般的です．
C++では「これを使え!!」というライブラリがありませんでしたが，PythonのMatplotlibをC++から使うライブラリである[matplotlib-cpp](https://github.com/lava/matplotlib-cpp)はMatplotlibを使いましょう．
matplotlib-cppは，Python2.7の環境が揃っていれば，matplotlibcpp.hをincludeし，ライブラリファイル(libpython2.7)をリンクすれば使えます．

## C++
### グラフ作成

#### 折れ線グラフ

#### 散布図

####


## Python
### グラフ作成

#### 折れ線グラフ

#### 散布図

####
