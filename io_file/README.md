# ファイルI/O （File I/O)
ターミナル(コンソール)を介したコンピュータとの対話(インタラクション)，例えばデータの入出力，はプログラミングを行う上で最も一般的な対話方法の一つです．
ここでは，ファイルI/Oについて学びます．

## 課題
C++, Pythonどちらの言語を使っても構わないので，以下の内容を実装してください．

* UCIのMachine Learning databaseの[Iris data](https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data)をダウンロードし，dataディレクトリに保存してください(CMakeを実行すると，このファイルがdataディレクトリにダウンロードされるように設定されています)．
* Iris dataは3種類の花(Iris-setosa, Iris-versicolor, Iris-virginica)の情報が各種類50株分ずつ保存されています．
* iris.dataファイルから1行ずつデータを読み込み，読み込んだデータを花の種類を基に3種類のファイルに保存じてください．
* 保存するファイル名は花の種類ごとに以下のように設定してください．
  * iris-setosa.data
  * iris-versicolor.data
  * iris-virginica.data


## C++
C++におけるファイルI/Oの基本的な内容は[このページ](http://www.cplusplus.com/doc/tutorial/files/)に分かりやすくまとめられています．
ファイルI/Oを行う場合，fstream](http://cpprefjp.github.io/reference/fstream.html)というヘッダファイルをincludeする必要があります．

fstreamヘッダは以下の機能を提供しています．
以下，順に紹介します(列挙の順序と説明の順序が異なりますが，気にしないでください)．
* ifstream: 入力ファイルストリーム
* ofstream: 出力ファイルストリーム
* fstream: 入出力ファイルストリーム

### 出力ファイルストリーム([std::ofstream](http://www.cplusplus.com/reference/fstream/ofstream/))
std::ofstreamはファイル上での操作を行うための出力ファイルストリームクラスです．
std::ofstream型のオブジェクトに対し，std::coutと同様<<operatorを使ってデータを書き出します．
```cpp
#include <fstream>
... (中略) ...
    std::ofstream ofs("my_text.txt");
    // ファイルオープンのエラーチェック
    if(ofs.fail()){
        std::cout << "File open failed." << std::endl;
    }
    // 文字列”Hello world"と改行の書き出し
    ofs << "Test" << std::endl;
    ofs << "Hello world" << std::endl;
    // 数値と改行の書き出し
    ofs << 1234 << std::endl;
    // 変数と書き出しの書き出し
    int x = 100;
    ofs << x << std::endl;
```
上記のコードを実行すると，my_text.txtというファイルが作成され，以下の内容が書き出されているはずです．<br>
Hello world<br>
1234<br>
100<br>

#### ファイル名の指定
ファイル名の指定方法は2種類あります．
一つ目の方法は，上記の例に書いてあるようにstd::ofstreamはコンストラクタでファイル名を指定する方法です．
もう一つの方法は，メンバ関数のopen()を使いファイル名を指定する方法です．

#### ファイルのopen/close
コンストラクタでファイル名を指定，もしくはopen()を呼ぶことでファイルを開けます．
ファイルを閉じるには，メンバ関数close()を呼び明示的にファイルを閉じるかデストラクタによって閉じる方法の2種類があります．

#### モード
ファイル名を指定する際に，第2引数でファイルのオープンモードを指定することができます．
以下に各キーワードの意味を列挙します．
* std::ofs::out: 読み込みモードで開く(デフォルト)．
* std::ofs::app: 書き出しの前にEOFに移動．
* std::ofs::ate: ファイルを開いた時に，EOFまで移動．
* std::ofs::trunc: 指定したファイルが存在する場合，既存ファイルに上書きする．
* std::ofs::binary: バイナリモード
ateはファイルオープン時のみにEOFに移動します．そのため，ファイルオープン後に現在位置を変更できます．
一方で，appは毎回書き出しをする前にEOFへ移動します．
```cpp
#include <fstream>
... (中略) ...
    // 追記モードでファイルを開く
    std::ofstream ofs_app("my_text_app.txt", std::ios::out | std::ios::app );
    // 追記モードでファイルを開く
    std::ofstream ofs_ate("my_text_ate.txt", std::ios::out | std::ios::ate );
    // 上書きモードでファイルを開く
    std::ofstream ofs_trunc("my_text_trunc.txt", std::ios::out | std::ios::trunc );
```

### 入力ファイルストリーム([std::ifstream](http://www.cplusplus.com/reference/fstream/ifstream/))
std::ifstreamはファイル上での操作を行うための入力ファイルストリームクラスです．
std::ifstream型のオブジェクトに対し，std::cinと同様>>operatorを使って変数にファイル内容を代入します．
```cpp
#include <fstream>
... (中略) ...
    std::ifstream ifs("my_text.txt");
    // ファイルオープンのエラーチェック
    if(ifs.fail()){
        std::cout << "File open failed." << std::endl;
    }

    // 文字列”Hello world"と改行の書き出し
    std::string str_wo_space, str_w_space;
    double value_d;
    int value_i;
    // 文字列の読み込み
    std::getline(ifs, str_wo_space);
    std::getline(ifs, str_w_space);
    // 実数の読み込み
    ifs >> value_d;
    // 整数の読み込み
    ifs >> value_i;

    // 出力
    std::cout << "Loaded string w/o space: " << str_wo_space << std::endl;
    std::cout << "Loaded string w/ space: " << str_w_space << std::endl;
    std::cout << "Loaded real number: " << value_d << std::endl;
    std::cout << "Loaded integer number: " << value_i << std::endl;
```
上記のコードを実行すると，ofstreamの説明で作成したmy_text.txtから読み込まれた内容が，以下のように出力されます．<br>
Loaded string w/o space: Test<br>
Loaded string w/ space: Hello world<br>
Loaded real number: 1234<br>
Loaded integer number: 100<br>

## Python
PythonのファイルI/Oを行う[open](http://docs.python.jp/3/library/functions.html#open)関数は，標準入出力関数と同様標準組み込み関数のため，特にパッケージをimportしなくても使用可能です．

open()はopen(file, mode='r', ...)と使用します．
第1引数にファイル名，第2引数にファイルを開くモードを指定します．
第2引数に指定する文字とモードの関係を以下に示します．
* 'r': 読み込みモードで開く(デフォルト)．
* 'w': 書き込みモードで開く．指定したファイルが存在する場合，既存ファイルに上書きする．
* 'x': 排他的(eXclusive)生成．指定したファイルが存在する場合はファイルオープンに失敗．
* 'a': 書き込みモードで開く．指定したファイルが存在する場合，ファイル末尾に追記(append)する．
* 't': テキストモード(デフォルト)
* 'b': バイナリモード

### ファイル書き出し: open(FILENAME, 'w')
open関数の第2引数を'w'とします．
追記(append)する場合は'wa'を指定してください．
```python
f = open('my_text_py.txt', 'w')

# 文字列の表示
f.write('Hello World!!\n')
# 数値の表示
f.write('1234\n')
# 変数の表示
x = 100
f.write(str(x)+"\n")

f.close()
```
上記のコードを実行すると，<br>
Hello World!!<br>
1234<br>
100<br>
という内容がmy_text_py.txtに書きだされます．
C++のofstreamと違い，明示的にファイルをクローズする必要があります．

### ファイル読み込み: open(FILENAME, 'r')
open関数の第2引数を'r'とします．
```python
# for Python 2
filename = 'my_text_py.txt'
if os.path.isfile(filename):
    f = open(filename, 'r')

    # 1行文読み込み，改行文字を削除
    line_str = f.readline().rstrip('\n')

    # 1行文読み込み，float型へキャスト
    value_real = float(f.readline())

    # 1行文読み込み，int型へキャスト
    value_int = int(f.readline())

    print "Loaded string:", line_str
    print "Loaded real number:", value_real
    print "Loaded integer number:", value_int

    f.close()
else:
    print filename, "does not exist"
```

```python
# for Python 3
filename = 'my_text_py.txt'
if os.path.exists(filename) and os.path.isfile(filename):
    f = open(filename, 'r')

    # 1行文読み込み，改行文字を削除
    line_str = f.readline().rstrip('\n')

    # 1行文読み込み，float型へキャスト
    value_real = float(f.readline())

    # 1行文読み込み，int型へキャスト
    value_int = int(f.readline())

    print("Loaded string:", line_str)
    print("Loaded real number:", value_real)
    print("Loaded integer number:", value_int)

    f.close()
else:
    print(filename, "does not exist")
```
os.path.exists()でfilenameというファイル・ディレクトリの存在を確認し，os.path.isfile()でfilenameというオブジェクトがファイルであることを確認します．<br>
上記のコードを実行すると，<br>
Loaded string: Hello World!!<br>
<br>
Loaded real number: 1234.0<br>
Loaded integer number: 100<br>
と表示されます．

### ファイル・ディレクトリ一覧の取得
pythonのosモジュールを使えば，指定したディレクトリ内のファイル・ディレクトリ一覧を取得可能です．

#### ファイル・ディレクトリ一覧
os.listdir()は指定したディレクトリ内のファイル，ディレクトリ名の一覧をlistとして返します．
例えば，**os.listdir(os.curdir)**と実行すると，<br>
['main_io_file_read.cpp',<br>
 'build',<br>
 'main_io_file_read.py',<br>
 'main_io_terminal2.cpp',<br>
 'CMakeLists.txt.user',<br>
 'my_text_py.txt',<br>
 'main_io_terminal2.py',<br>
 'CMakeLists.txt',<br>
 'main_io_terminal1.py',<br>
 'main_io_terminal1.cpp',<br>
 'main_io_file_write.py',<br>
 'main_io_file_write.cpp']<br>
 とlistが返されます．

#### ワイルドカードの指定
ワイルドカードを指定する場合は，globモジュールのglob()を使います．
例えば，先ほどと同じディレクトリで**glob.glob(os.curdir+"/*.cpp")**と実行すると<br>
['./main_io_file_read.cpp',<br>
 './main_io_terminal2.cpp',<br>
 './main_io_terminal1.cpp',<br>
 './main_io_file_write.cpp']<br>
 とlistが返されます．
