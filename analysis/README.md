# データ解析 (data analysis)
ここでは簡単なデータ解析と解析結果の可視化について学びます．
C++ではstd::vector型オブジェクトに格納されたデータ，Pythonではnumpy.array型オブジェクトに格納されたデータの解析について説明します．

## C++
コンテナに対する様々な処理を実装している[*algorithm*](http://www.cplusplus.com/reference/algorithm/)に加え，数値データを扱う[*numeric*](http://www.cplusplus.com/reference/numeric/)が有用です．

### 合計値
std::accumulate(first, last, init): 初期値initに2つのイテレータ[first, last]の間の全要素の合計値を足した数値を返します．

### L2 norm
[*std::inner_product*](http://www.cplusplus.com/reference/numeric/inner_product/)(first1, last1, first2, init): 初期値initに2つのstd::vector型オブジェクトの内積を足した数値を返します．
first1，last1の2つのイテレータで片方のゔvectorを指定すると共に内積を計算する要素数を確定します．
first2によってもう片方のvectorを指定します．
first1とfirst2を同じイテレータにすることにより，1つのvector型オブジェクトのL2-normが計算できるわけです．
```cpp
#include <vector>
#include <numeric>
int main()
{
    // set v[n] = n
    size_t size = 10;
    std::vector<double> v;
    for(size_t n = 0; n < size; ++n)
    {
         v[n] = static_cast<double>(n);  
    }

    /// compute mean
    double l2_norm = double variance = std::inner_product(v.begin(), v.end(), v.begin(), 0.0);

    std::cout << "L2 norm of v: " << l2_norm << std::endl;

    return 0;
}
```

### 平均値と分散，標準偏差
std::accumulate(first, last, init)/double(size): 合計値を要素数で割った値を計算する．
```cpp
#include <vector>
#include <numeric>
int main()
{
    // set v[n] = n
    size_t size = 10;
    std::vector<double> v;
    for(size_t n = 0; n < size; ++n)
    {
         v[n] = static_cast<double>(n);  
    }

    /// compute mean
    double sum = std::accumulate(v.begin(), v.end(), 0.0);
    double mean = sum / size;

    /// compute standard deviation
    // v_normed[i] = v[i] - mean
    std::vector<double> v_normed(v.size());
    std::transform(v.begin(), v.end(), v_normed.begin(), std::bind2nd(std::minus<double>(), mean));
    // variance = sum(v_normed[0]^2, ..., v_normed[9]^2)
    double variance = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
    // standard_deviation
    double standard_deviation = std::sqrt(variance / size);

    std::cout << "sum: " << sum << std::endl;
    std::cout << "mean: " << mean << std::endl;
    std::cout << "variance: " << variance << std::endl;
    std::cout << "standard_deviation: " << standard_deviation << std::endl;

    return 0;}
```

## Python

### 合計値
numpy.array型オブジェクトの合計値は，numpy.array.sum(arr, axis=None)， arr.sum(axis=None)のどちらでも計算できます．
axisによって合計を計算する軸を指定します．
Noneであれば全要素の合計値となります．
```python
# for Python 2
import numpy as np

x = np.arange(6).reshape(3,2)
print 'array:\n', x
print 'sum:', x.sum()
print 'sum along y axis:', x.sum(axis=0)
print 'sum along x axis:', x.sum(axis=1)
```

```python
# for Python 3
import numpy as np

x = np.arange(6).reshape(3,2)
print('array:\n', x)
print('sum:', x.sum())
print('sum along y axis:', x.sum(axis=0))
print('sum along x axis:', x.sum(axis=1))
```
上記のコードを実行すると，以下のような結果が得られます．<br>
array:<br>
[[0 1]<br>
 [2 3]<br>
 [4 5]]<br>
sum: 15<br>
sum along y axis: [6 9]<br>
sum along x axis: [1 5 9]<br>

### 平均値と分散，標準偏差
numpy.array型オブジェクトの平均値は，numpy.array.mean(arr, axis=None)， arr.mean(axis=None)のどちらでも計算できます．
axisによって平均を計算する軸を指定します．
Noneであれば全要素の平均値となります．
```python
# for Python 2
import numpy as np

x = np.arange(6).reshape(3,2)
print 'array:\n', x
print 'mean:', x.mean()
print 'mean along y axis:', x.mean(axis=0)
print 'mean along x axis:', x.mean(axis=1)
```

```python
# for Python 3
import numpy as np

x = np.arange(6).reshape(3,2)
print('array:\n', x)
print('mean:', x.mean())
print('mean along y axis:', x.mean(axis=0))
print('mean along x axis:', x.mean(axis=1))
```
上記のコードを実行すると，以下のような結果が得られます．<br>
array:<br>
[[0 1]<br>
 [2 3]<br>
 [4 5]]<br>
mean: 2.5<br>
mean along y axis: [ 2.  3.]<br>
mean along x axis: [ 0.5  2.5  4.5]<br>

### 標準偏差
numpy.array型オブジェクトの標準偏差は，numpy.array.std(arr, axis=None)， arr.std(axis=None)のどちらでも計算できます．
axisによって標準偏差を計算する軸を指定します．
Noneであれば全要素の標準偏差となります．
```python
# Python 2
import numpy as np

x = np.arange(6).reshape(3,2)
print 'array:\n', x
print 'standard deviation:', x.std()
print 'standard deviation along y axis:', x.std(axis=0)
print 'standard deviation along x axis:', x.std(axis=1)
```

```python
# Python 3
import numpy as np

x = np.arange(6).reshape(3,2)
print('array:\n', x)
print('standard deviation:', x.std())
print('standard deviation along y axis:', x.std(axis=0))
print('standard deviation along x axis:', x.std(axis=1))
```
上記のコードを実行すると，以下のような結果が得られます．<br>
array:<br>
[[0 1]<br>
 [2 3]<br>
 [4 5]]<br>
standard deviation: 1.70782512766<br>
standard deviation along y axis: [ 1.63299316  1.63299316]<br>
standard deviation along x axis: [ 0.5  0.5  0.5]<br>

### ノルム
numpy.array型オブジェクトのノルムは，numpy.linalg.norm(arr, ord=None, axis=None)で計算できます．
ordは
* None: ベクトルであればL2ノルム，行列であればFrobeniusノルムを返します．
* ‘fro’: 行列のFrobeniusノルムを返します．
* ‘nuc’: 行列のnuclearノルム(核ノルム)を返します．
* inf: ベクトルであれば最大値，行列であれば行毎の最大値を返します．
* -inf: ベクトルであれば最小値，行列であれば行毎の最小値を返します．
* 0: ベクトルの非ゼロ要素の数を返します．
* 1: ベクトルであればL1ノルム，行列であれば列ごとの最大値を返します．
* －1: ベクトルであればL1ノルム，行列であれば列ごとの最小値を返します．
* 2: ベクトルであればL2ノルム，行列であれば最大特異値を返します．
* －2: ベクトルであればL1ノルム，行列であれば最小特異値を返します．
* 上記以外の値: ベクトルのLpノルム(pは指定された値)を返します．
