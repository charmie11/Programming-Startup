#!/bin/bash
CUR_DIR=`dirname $0`
$CUR_DIR/build/main_parser arg1

echo run main_parser with 1, 2, ..., 5
for var in `seq 1 5`
do
	$CUR_DIR/build/main_parser $var
done

echo run main_parser with 10, 20, ..., 50
var=10
until [ $var -gt 50 ]; do
	$CUR_DIR/build/main_parser $var
	let var+=10
done

echo run main_parser with each .cpp file as its argument
for file in *.cpp;
do
	$CUR_DIR/build/main_parser $file
done
