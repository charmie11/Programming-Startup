#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

import random
import numpy as np


def rand_uniform_random(size=10):
    a_int = 3
    b_int = 6
    a_real = 2.7
    b_real = 9.1

    for n in range(size):
        print(random.randint(a_int, b_int),
              ",",
              random.uniform(a_real, b_real))


def rand_normal_random(size=10):
    mean = 3.5
    sigma = 1.2

    for n in range(size):
        print(random.normalvariate(mean, sigma))

    return 0


def rand_uniform_numpy():
    mean = 3.5
    sigma = 1.2

    print(sigma * np.random.randn(3, 2) + mean)


def rand_normal_numpy():
    a = 2.7
    b = 9.1

    print((b - a) * np.random.rand(3, 2) + a)


if __name__ == "__main__":

    rand_uniform_random()
    rand_normal_random()
    rand_uniform_numpy()
    rand_normal_numpy()
