#include <iostream>
#include <vector>
#include <sstream>

int main(int argc, char* argv[])
{
    std::vector<std::string> words;
    std::string line, word;
    char delim{' '};

    // store the input as a string
    std::cout << "Please type a long sentence that includes some white spaces: " << std::flush;
    std::getline(std::cin, line);

    // split the input string into vector<string> with delimiter ' '
    std::istringstream iss(line);
    while(std::getline(iss, word, delim))
    {
        words.push_back(word);
    }

    // show the number of words.
    std::cout << "Your input data has " << words.size() << " words." << std::endl;
    // show the input data
    for(size_t i = 0; i < words.size(); ++i)
    {
        std::cout << "words[" << i << "] = " << words[i] << std::endl;
    }
}