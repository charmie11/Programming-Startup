#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import os


if __name__ == "__main__":

    filename = 'my_text_py.txt'
    if os.path.isfile(filename):
        f = open(filename, 'r')

        # 1行文読み込み，改行文字を削除
        line_str = f.readline().rstrip('\n')

        # 1行文読み込み，float型へキャスト
        value_real = float(f.readline())

        # 1行文読み込み，int型へキャスト
        value_int = int(f.readline())

        print("Loaded string:", line_str)
        print("Loaded real number:", value_real)
        print("Loaded integer number:", value_int)

        f.close()
    else:
        print(filename, "does not exist")
