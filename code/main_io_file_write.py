#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function


if __name__ == "__main__":

    f = open('my_text_py.txt', 'w')

    # 文字列の書き出し
    f.write('Hello World!!\n')

    # 数値の書き出し
    f.write('1234\n')

    # 変数の書き出し
    x = 100
    f.write(str(x)+"\n")

    f.close()
