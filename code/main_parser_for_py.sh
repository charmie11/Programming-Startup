#!/bin/bash

echo run main_parser with 1, 2, ..., 5
for var in `seq 1 5`
do
	python main_parser.py $var
done

echo run main_parser with 10, 20, ..., 50
var=10
until [ $var -gt 50 ]; do
	python main_parser.py $var
	let var+=10
done

echo run main_parser with each .cpp file as its argument
for file in *.cpp;
do
	python main_parser.py $file
done