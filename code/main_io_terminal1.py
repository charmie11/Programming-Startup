#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function


if __name__ == "__main__":

    # set name
    print("Please type your name: ")
    name = input()

    # set age
    print("Please type your age: ")
    age = eval(input())

    # show the input data
    print("You are", name, "and", age, "years old.")
