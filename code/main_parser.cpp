#include <iostream>

int main(int argc, char* argv[]){
	std::cout << "The number of arguments: " << argc << std::endl;
	for(int n = 0; n < argc; ++n)
	{
		std::cout << "arg[" << n << "]: " << argv[n] << std::endl;
	}

	return 0;
}