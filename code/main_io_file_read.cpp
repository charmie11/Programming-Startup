#include <iostream>
#include <string>
#include <fstream>

int main(int argc, char* argv[])
{
    std::string name{"my_text.txt"};

    std::ifstream ifs(name.c_str());
    // ファイルオープンのエラーチェック
    if(ifs.fail()){
        std::cout << "File open failed." << std::endl;
    }

    // 文字列”Hello world"と改行の書き出し
    std::string str_wo_space, str_w_space;
    double value_d;
    int value_i;
    // 文字列の読み込み
    std::getline(ifs, str_wo_space);
    std::getline(ifs, str_w_space);
    // 実数の読み込み
    ifs >> value_d;
    // 整数の読み込み
    ifs >> value_i;

    // 出力
    std::cout << "Loaded string w/o space: " << str_wo_space << std::endl;
    std::cout << "Loaded string w/ space: " << str_w_space << std::endl;
    std::cout << "Loaded real number: " << value_d << std::endl;
    std::cout << "Loaded integer number: " << value_i << std::endl;

    ifs.close();

    return 0;
}
