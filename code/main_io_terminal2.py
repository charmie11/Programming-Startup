#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function


if __name__ == "__main__":

    # get words
    print("Please type a long sentence that includes some white spaces: ")
    words = input().split()

    # show the number of the input words
    print("Your input data has", len(words), "words.")
    for i in range(len(words)):
        print("words[", i, "] =", words[i])
