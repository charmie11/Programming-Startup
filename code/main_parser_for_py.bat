@echo run main_parser with 1, 2, ..., 5
for /L %%i in (1,1,5) do python main_parser.py %%i

@echo run main_parser with 10, 20, ..., 50
for /L %%i in (10,10,50) do python main_parser.py %%i

@echo run main_parser with each .cpp file as its argument
for /f "usebackq" %%i in (`dir /B *.cpp`) do python main_parser.py %%i