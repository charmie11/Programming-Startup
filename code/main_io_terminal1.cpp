#include <iostream>
#include <string>

int main(int argc, char* argv[])
{
    std::string name;
    int age;

    // set name
    std::cout << "Please type your name: " << std::flush;
    std::cin >> name;

    // set age
    std::cout << "Please type your age: " << std::flush;
    std::cin >> age;

    // show the input data
    std::cout << "You are " << name << " and " << age << " years old." << std::endl;

    return 0;
}