#include <iostream>
#include <fstream>

int main(int argc, char* argv[])
{
    std::string name{"my_text.txt"};

    std::ofstream ofs(name.c_str());
    // ファイルオープンのエラーチェック
    if(ofs.fail()){
        std::cout << "File open failed." << std::endl;
    }

    // 文字列”Hello world"と改行の書き出し
    ofs << "Test" << std::endl;
    ofs << "Hello world" << std::endl;
    // 数値と改行の書き出し
    ofs << 1234 << std::endl;
    // 変数と書き出しの書き出し
    int x = 100;
    ofs << x << std::endl;

    ofs.close();

    return 0;
}