﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import sys


if __name__ == "__main__":

    # コマンドライン引数のリストを取得
    argvs = sys.argv
    # コマンドライン引数の総数を取得
    argc = len(argvs)
    print("The number of arguments:", argc)
    n = 0
    for n, argv in enumerate(argvs):
        print("arg[%d]: %s" % (n, argv))
