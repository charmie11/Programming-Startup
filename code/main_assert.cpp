#include <iostream>
#include <cassert>

void func(const int x)
{
    assert(x >= 0 && "The input variable x must be non-negative value.");
    std::cout << "x is " << x << std::endl;
}

int main()
{
    func(0);
    func(3);
    func(-3); // program fails

    return 0;
}