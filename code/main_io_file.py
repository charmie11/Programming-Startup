#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import os


if __name__ == "__main__":

    filename = 'data/iris.data'
    if not os.path.isfile(filename):
        print(filename, "does not exist")

# prepare output files
    classes = ['Iris-setosa', 'Iris-versicolor', 'Iris-virginica']
    files = [open('data/iris-setosa.data', 'w'),
             open('data/iris-versicolor.data', 'w'),
             open('data/iris-virginica.data', 'w')]

# process each line
    f = open(filename, 'r')
    while True:
        # read a line
        line = f.readline().rstrip('\n')
        if not line:
            break

        # decompose the line with ','
        sepal_length, sepal_width, petal_length, petal_width, \
            class_name = line.split(',')

        # find the index of classes
        class_index = classes.index(class_name)

        # write the line w/o class name
        files[class_index].write(line.rstrip(class_name)+"\n")

    f.close()

    # close all files
    for f in files:
        f.close()
