#include <iostream>
#include <random>


void random_normal(
	double mean = 0.0,
	double stddev = 1.0,
	const size_t size = 100
)
{
	// random value generator
    std::random_device seed_gen;
    std::default_random_engine engine(seed_gen());
	
    std::normal_distribution<> dist(mean, stddev);
    for(size_t n = 0; n < size; ++n)
    {
        std::cout << dist(engine) << std::endl;
    }
}

void random_uniform_int(
	const int a = 0,
	const int b = 100,
	const size_t size = 100
)
{
	// random value generator
    std::random_device seed_gen;
    std::default_random_engine engine(seed_gen());
	
    std::uniform_int_distribution<> dist(a, b);
    for(size_t n = 0; n < size; ++n)
    {
        std::cout << dist(engine) << std::endl;
    }
}

void random_uniform_real(
	const double a = 0,
	const double b = 100,
	const size_t size = 100
)
{
	// random value generator
    std::random_device seed_gen;
    std::default_random_engine engine(seed_gen());
	
    std::uniform_real_distribution<> dist(a, b);
    for(size_t n = 0; n < size; ++n)
    {
        std::cout << dist(engine) << std::endl;
    }
}

int main()
{
    random_normal(3.0, 1.0);
	random_uniform_int(4,14);
	random_uniform_real();

    return 0;
}
