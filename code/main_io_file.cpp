#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

size_t FindClassIndex(const std::string class_name)
{
    size_t index = 0;
    if(!class_name.compare("Iris-setosa"))
    {
        index = 1;
    }
    else if(!class_name.compare("Iris-versicolor"))
    {
        index = 2;
    }
    else if(!class_name.compare("Iris-virginica"))
    {
        index = 3;
    }

    return index;
}

int main(int argc, char* argv[])
{
    std::string dirname{"../data/"};
    std::string name{dirname+"iris.data"};

    std::ifstream ifs(name.c_str());
    if(ifs.fail()){
        std::cout << "Cannot load " << name << std::endl;
    }

    // prepare output files
    std::ofstream ofs_setosa(dirname+"iris-setosa.data");
    std::ofstream ofs_versicolor(dirname+"iris-versicolor.data");
    std::ofstream ofs_virginica(dirname+"iris-virginica.data");

    std::string line;
    std::string line_data;
    std::string class_name;
    size_t class_index;
    // process each line
    while(!ifs.eof() && std::getline(ifs, line))
    {
        if(line.length() == 0)
        {
            break;
        }
        // check the class
        auto index_delim = line.find_last_of(',');
        class_name = line.substr(index_delim+1);
        class_index = FindClassIndex(class_name);
        line_data = line.substr(0, index_delim);
        switch(class_index)
        {
            case 1:
                ofs_setosa << line_data << std::endl;
                break;
            case 2:
                ofs_versicolor << line_data << std::endl;
                break;
            case 3:
                ofs_virginica << line_data << std::endl;
                break;
            default:
                std::cout << "Undefined class..." << std::endl;
        }
    }

    ifs.close();
    ofs_setosa.close();
    ofs_versicolor.close();
    ofs_virginica.close();

    return 0;
}
