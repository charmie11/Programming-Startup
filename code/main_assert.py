#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function


def func(x):
    assert x >= 0, "The input variable x must be non-negative value."
    print("x is", x)


if __name__ == "__main__":

    func(0)
    func(3)
    func(-3)  # program fails
