# 可読性と使い易さのためのコメント (Comments for readability and usability)
面倒くさいという理由で敬遠しがちなコメントの記述ですが，コードの読みやすさを向上させるためには必要不可欠な要素になります．
人や団体，言語によってコメントの書き方のルールに違いがありますが，ここではGoogle Style Guideの内容を読みながらダラダラと書いたメモを記載します．

興味がある人は，ぜひ本家のサイトを参照してください．
* Google Style Guide for C++ ([原文](https://google.github.io/styleguide/cppguide.html#Comments)，[和訳](http://www.textdrop.net/google-styleguide-ja/cppguide.xml#%E3%82%B3%E3%83%A1%E3%83%B3%E3%83%88))
* Google Style Guide for Python ([原文](https://google.github.io/styleguide/pyguide.html#Comments)，[和訳](http://works.surgo.jp/translation/pyguide.html#id59))

## 課題
C++, Pythonどちらの言語を使っても構わないので，以下の内容を実装してください．
* 今まで書いたプログラムにコメントを追加する．

## ドキュメント生成ツール
ドキュメンテーション生成ツールを使用すると良いでしょう．
ドキュメンテーション生成ツールは，コード内に記載されたコメントを読み取り，様々な形式でドキュメンテーションの文書を生成します．

例えば，C++やPythonであればDoxygen ([英語](http://www.stack.nl/~dimitri/doxygen/)，[和文](http://www.doxygen.jp/))などがあります．


## 変数名
変数名はその役割が想像ができるような名前をつけてください．
その上で，必要であれば以下のような情報を記載すると良いでしょう．
* どのような処理に使われるか
* 何かしらの特殊な意味を持っているのか

## 関数
関数に対するコメントには，宣言に対するコメントと定義に対するコメントの2種類があります．

### 宣言
関数宣言のコメントでは，関数の使い方を書きます．
具体的には，以下の項目を記述すると良いでしょう．
* 関数の簡単な説明
* 入力引数
* 返戻値
* 例外など
関数宣言のコメントを書くポイントは，その関数の設計を何も知らない人が(1)正しい引数を与えられる，(2)正しい型の返戻値を受け取れる，(3)入力引数や返戻値に関する例外や制約が理解できる程度に書くことです．

例えば，Pythonでは関数宣言部のコメントを以下のように書くよう推奨されています．
* Args: 各引数の一覧．各変数は変数名，コロン，スペース，説明という順番で記載．
* Returns: 返戻値の一覧．返戻値が無い，もしくはNoneの場合は記載しない．
* Raises: 入出力に関する例外の一覧．
```python
def function_name(input_args_1, input_args_2, input_args_3=None):
    """ Do something.

    Retrieves rows pertaining to the given keys from the Table instance
    represented by big_table.  Silly things may happen if
    other_silly_variable is not None.

    Args:
        input_args_1: Input data for this process
        input_args_2: A set of parameters for this process
        input_args_3: Optional variable,
            default value is None

    Returns:
        The processed result as a 2 dimensional numpy.array.

    Raises:
        IOError: An error occurred accessing the input_args_1 object.
    """
```

### 定義
関数定義のコメントでは，関数内部の具体的な処理を理解するために必要な説明を書きます．
具体的には，以下の項目を記述すると良いでしょう．
* (高速化や効率化のための)特殊な実装方法であったり，処理の流れについて説明すると良いでしょう．
* 参考にした書籍，論文などの文献情報
関数定義のコメントを書くポイントは，自分がもう一度実装する時に知っておきたい情報を書くことです．

## TODO
将来的に修正・更新をしたい箇所にはTODOコメントを書きましょう．
何かしらの理由で非効率な実装をしてしまったり，挙動が怪しい，例外処理ができていない時など，今後修正を行う予定の箇所に，以下のような情報を記載しましょう．
* 担当者の名前やメールアドレス
* 放置した際に生じる影響
* 修正内容
* 修正予定日
