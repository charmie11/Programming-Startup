# テスト (test)
自分で書いたプログラムが正しく動作するか検証することをテストと呼びます．
特に，個々のモジュールに対して行うテストをunit test(単体テスト)と呼びます．
単体テストを行うことで，プログラムのバグ発見，リファクタリングが容易になるといった利点が挙げられます．
ここでは，Unit Test FrameworkであるGoogle Test(C++)とunittest(Python)を使って，単体テストの概念・方法を知ると共に，単体テストを習慣化しましょう．

## 課題
C++, Pythonどちらの言語を使っても構わないので，以下の内容を実装してください．

## C++
C++では[Google Test](https://github.com/google/googletest)を使用します．
Google TestはC++用のユニットテストフレームワークです．

Boost.Testはheader only library

## Python
PythonではPython用のユニットテストフレームワークである[unittest](http://docs.python.jp/2/library/unittest.html)モジュールを使用します．
unittestは以下のような機能を提供しています．
* テストの自動化
* 初期設定と終了処理の共有
* テストの分類
* テスト実行と結果レポートの分離
上記の機能を提供するため，unittestは以下の概念をサポートしています．
* test fixture: test fixtureは(1つもしくは複数の)テストの実行に必要な準備や終了処理を表します．例えば一時的なデータベース，ディレクトリの作成やサーバー処理の開始などです．
* test case: test caseはテストの最小単位です．入力に対する挙動を確認します．unittestは基本クラスであるTestCaseを提供しています．
* test suite: test suiteは複数のtest caseや複数のtest suite，もしくは両者をまとめたものです．同時に実行するべきテストをまとめて扱う時に使います．
* test runner: test runnerはテストの実行を行いテスト結果を提供します．GUIやCUIの提供やテストの実行結果を表す何かしらの値を返します．

スクリプトファイル内からテストを実行するためには *unittest.main()* を実行します．
1. 同一pyファイル内に存在するunittest.TestCaseを継承した全クラスがsuiteとして認識され，
* 関数名の先頭が *test_* で始まるメソッドのみがtest caseとして実行されます．

ターミナルから実行する場合，以下のようなコマンドを使います．
* python file_for_test.py -v # file_for_test.pyファイル内の全テストケースを実行し，テストの詳細を表示
* python file_for_test.py # file_for_test.pyファイル内の全テストケースを実行
* python -m unittest file_for_test.py.specific_test -v # file_for_test.pyファイル内のspecific_testというテストのみを実行
* python -m unittest discover # pyファイルを指定せずまとめて実行

### test case
unittestの使い方は
* unittest.TestCaseクラスを継承したクラスにテストメソッドを記述
* テストメソッドの関数名は *test_* で始まる
* 1つ以上のassert用の命令( *self.assertMETHOD* )が必要です．METHODにはEqualやTrueなど前提条件を記述します．

```python
```
上記のコードを実行すると，以下のようにターミナルに出力されます．

### test fixture
複数のtest caseで共通する初期化・終了処理をまとめる時に使います．
* 初期化はsetup()，終了処理はtearDown()に記述します．
* 初期化，終了処理は各テストが実行されるたびに呼ばれます．
