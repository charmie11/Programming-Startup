# 配列 (arrays)
ここでは配列の使い方を学びます．
C++では *std::vector* ，Pythonでは *numpy.array* による配列の扱い方について学びましょう．

## C++
C++の標準ライブラリの一つである *std::vector* は，以下の機能を持つ動的配列の一つです．
* 要素の追加・削除
* 特定要素へのアクセス
* 特定の条件を満たす要素の検索
* 配列全体に対する処理 (*algorithm* の使用)

### 初期化
std::vectorはtemplate型で定義されているので，オブジェクトの宣言時に型指定をする必要があります．
既存のオブジェクトの値をコピーするコピーコンストラクタや，要素数を指定したコンストラクタが提供されています．
```cpp
#include <vector>

int main()
{
    // 様々な型のvector型オブジェクトの宣言
    std::vector<int> v_int;
    std::vector<float> v_float;
    std::vector<double> v_double;
    std::vector<std::string> v_string;

    // コピーコンストラクタ
    std::vector<int> v_int_2(v_int);

    // 要素数を指定した宣言
    std::vector<int> v_int_3(5);

    std::cout << "size of v_int: " << v_int.size() << std::endl;
    std::cout << "size of v_int_2: " << v_int_2.size() << std::endl;
    std::cout << "size of v_int_3: " << v_int_3.size() << std::endl;

    return 0;
}
```

### 要素の追加
配列へのデータの追加はpush_back()を使います．
std::vector型オブジェクトvに対してv.push_back(element)と実行すると，vの末尾にelementという要素が追加されます．

### 要素へのアクセス
std::vector型オブジェクトへのアクセス方法は，静的配列と同じ要素のindexを遣ったアクセスと[イテレータ](http://ppp-lab.sakura.ne.jp/cpp/library/012.html)を遣ったアクセス方法の2種類があります．
```cpp
#include <vector>

int main()
{
    std::vector<int> v;
    for(int n = 0; n < 10; ++n)
    {
        v.push_back(n);
    }

    // 配列の各要素へのアクセス(要素のindexを使用)
    for(size_t n = 0; n < 10; ++n)
    {
        std::cout << "v[" << n << "] = " <<  v[n] << std::endl;
    }

    // 配列の各要素へのアクセス(iteratorを使用)
    for(std::vector<int>::iterator it = v.begin(); it != v.end(); ++it)
    {
        std::cout << "v[" << it-v.begin() << "] = " <<  \*it << std::endl;
    }

    return 0;
}
```

### 要素の削除
配列の要素の削除には，pop_back()，erase()はpush_back()を使います．

pop_back()関数はposu_back()の逆の操作を行います．
すなわち，std::vector型オブジェクトvに対してv.pop_back()と実行すると，vの末尾の要素が削除されます．

erase()関数は，特定の要素を削除するための関数です．
引数にイテレータを1つ渡す場合は，そのイテレータが指す要素のみを削除し，
2つ渡す場合は，2つのイテレータで定義される範囲内の要素が削除されます．
```cpp
#include <vector>

void print_vector(const std::vector<int> v)
{
    for(size_t n = 0; n < v.size(); ++n)
    {
        std::cout << "v[" << n << "] = " <<  v[n] << std::endl;
    }
}

int main()
{
    std::vector<int> v;
    for(int n = 0; n < 10; ++n)
    {
        v.push_back(n);
    }
    std::cout << "push_backed 10 elements" << std::endl;
    print_vector(v);

    // pop_back
    v.pop_back();
    v.pop_back();
    std::cout << "pop_backed 2 elements" << std::endl;
    print_vector(v);

    // erase with a single argument
    v.erase(v.begin() + 5);
    std::cout << "erased 5th element" << std::endl;
    print_vector(v);

    // erase with 2 arguments
    v.erase(v.begin() + 1, v.begin() + 3);
    std::cout << "erased 1-3th elements" << std::endl;
    print_vector(v);

    return 0;
}
```

### 配列に対する処理
[*algorithm*](http://www.cplusplus.com/reference/algorithm/)はコンテナに対する様々な処理が実装された関数を提供します．

*all_of(first, last, pred), any_of(first, last, pred), none_of(first, last, pred)*<br>
2つのイテレータ[first, last]の間の
* all_of: 全要素が条件predを満たせば
* any_of: 少なくとも1つの要素が条件predを満たせば
* none_of: 全ての要素が条件predに対してFalseならば
Trueを返す．

*find(first, last, val), find_if(first, last, pred)*<br>
2つのイテレータ[first, last]の間の要素で
* find: valと同じ値を持つ
* find_if: 条件predを満たす
最初の要素のiteratorを返す．

*count（first, last, val), count_if(first, last, pred)*<br>
2つのイテレータ[first, last]の間の要素で
* find: valと同じ値を持つ
* find_if: 条件predを満たす
要素の数を返す．

*fill(first, last, val)*<br>
2つのイテレータ[first, last]の間の要素の値をvalに設定する．

*replace(first, last, old_value, new_value)*<br>
2つのイテレータ[first, last]の間の要素で値がold_valueである要素の値をold_valueに置換する．

*random_shuffle（first, last, gen）*<br>
2つのイテレータ[first, last]の間の要素の値をランダムに入れ替える．
genが引数として指定された場合，指定された関数ポインタ/関数オブジェクトが指す関数を使って要素の入れ替えを行う．
genが指定されない場合はbuilt-inの関数を使用して要素の入れ替えを行う．

*sort(first, last)*<br>
2つのイテレータ[first, last]の間の要素を昇べきの順に並び替える．
第3引数で要素の比較関数の関数ポインタ/関数オブジェクトを指定すると，指定した関数による比較を行う．

*is_sorted(first, last)*<br>
2つのイテレータ[first, last]の間の要素が昇べきの順になっていればTrueを返す．

*lower_bound(first, last, val), upper_bound(first, last, val)*<br>
sort済みのコンテナに対するアルゴリズムです．<br>
2つのイテレータ[first, last]の間の要素の内，
* lower_bound: 指定されたval以上の値が現れる
* upper_bound: 指定されたvalより大きい値が現れる
最初の要素のイテレータを返す．

*set_union(first1, last1, first2, last2, retult), set_intersection(first1, last1, first2, last2, retult), set_difference(first1, last1, first2, last2, retult)*<br>
sort済みの2つのコンテナに対するアルゴリズムです．<br>
[first1, last1]，[first2, last2]の2つのsort済みコンテナの
* set_union: 集合和を
* set_intersection: 集合積を
* set_difference: 差集合を
resultによって表されるコンテナに格納する．
```cpp
#include <vector>
#include <algorithm>
#include <iostream>

void print_vector(const std::vector<int> v)
{
    for(size_t n = 0; n < v.size(); ++n)
    {
        std::cout << "v[" << n << "] = " <<  v[n] << std::endl;
    }
}

void try_set_X(const int flag)
{
    std::vector<int> first = {5, 20, 15, 10, 25};
    std::vector<int> second = {50, 40, 30, 20, 10};
    std::vector<int> v(10);

    // sort the arrays
    std::sort(first, first+5);
    std::sort(second, second+5);
    std::cout << "First array: "; print_vector(first);
    std::cout << "Second array: "; print_vector(second);

    switch(flag)
    {
        case 1: // set_intersection
        std::cout << "Set intersection : ";
        it = std::set_intersection(first, first+5, second, second+5, v.begin());
        break;
        case 2: // set_difference
        std::cout << "Set intersection : ";
        it = std::set_difference(first, first+5, second, second+5, v.begin());
        break;
        default: // set_union
        std::cout << "Set union: ";
        it = std::set_union(first, first+5, second, second+5, v.begin());
    }
    v.resize(it-v.begin());
    print_vector(v);
}

int main()
{
    try_set_X(0);
    try_set_X(1);
    try_set_X(2);

    return 0;
}
```

*min_element(first, last), /max_element(first, last)*<br>
2つのイテレータ[first, last]の間の要素の
* min_element: 最小値
* max_element: 最大値
を持つ要素のイテレータを返す．

## Python
Pythonで配列を扱う最も一般的なオブジェクトはN次元配列を扱うための *numpy.ndarray* です．<br>
C++の *std::vector* とは異なり静的配列ではあるものの，様々な関数が用意されているので，使い方に慣れると良いでしょう．

### 初期化
numpy.array型オブジェクトの[生成関数](https://docs.scipy.org/doc/numpy/reference/routines.array-creation.html)は複数あります．
* empty(shape[, dtype, order]): 各要素の値を初期化せずに指定されたshapeのarrayを返す．
* eye(N[, M, k, dtype]): 対角成分が1，額各成分が0となる行列を返す．1引数の場合，N x N行列を返し，2引数の場合N x M行列を返す．第3引数kは対角成分のindexを表す．k=0で対角成分を1とし，kが正の値なら対角成分を右上にずらし，婦の値なら左下にずらす．
* identity(n[, dtype]): サイズがn x nの単位行列を返す．
* ones(shape[, dtype, order]): 全要素の値が1となる指定されたshapeのarrayを返す．
* zeros(shape[, dtype, order]): 全要素の値が0となる指定されたshapeのarrayを返す．
* full(shape, fill_value[, dtype, order]): 全要素の値がfill_valueとなる指定されたshapeのarrayを返す．
```python
# for Python 2
import numpy as np

print 'np.empty:'
print np.empty((3,2))
print 'np.eye:'
print np.eye(3)
print np.eye(3, 4)
print np.eye(5, k=2)
print np.eye(5, k=-1)
print 'np.identity:'
print np.identity(4)
print 'np.ones and np.zeros:'
print np.ones((3,2))
print np.zeros((2,3))
print 'np.full:\n'
print np.full((5,4), 7.0)
```

```python
# for Python 3
import numpy as np

print('np.empty:\n', np.empty((3,2)))
print('np.eye:\n', np.eye(3))
print(np.eye(3, 4))
print(np.eye(5, k=2))
print(np.eye(5, k=-1))
print('np.identity:\n', np.identity(4))
print('np.ones and np.zeros:\n', np.ones((3,2)))
print(np.zeros((2,3)))
print('np.full:\n', np.full((5,4), 7.0))
```
上記のコードを実行すると，下記のような結果が得られます．<br>
np.empty: <br>
[[ 0.  0.] <br>
 [ 0.  0.] <br>
 [ 0.  0.]] <br>
np.eye: <br>
[[ 1.  0.  0.] <br>
 [ 0.  1.  0.] <br>
 [ 0.  0.  1.]] <br>
[[ 1.  0.  0.  0.] <br>
 [ 0.  1.  0.  0.] <br>
 [ 0.  0.  1.  0.]] <br>
[[ 0.  0.  1.  0.  0.] <br>
 [ 0.  0.  0.  1.  0.] <br>
 [ 0.  0.  0.  0.  1.] <br>
 [ 0.  0.  0.  0.  0.] <br>
 [ 0.  0.  0.  0.  0.]] <br>
[[ 0.  0.  0.  0.  0.] <br>
 [ 1.  0.  0.  0.  0.] <br>
 [ 0.  1.  0.  0.  0.] <br>
 [ 0.  0.  1.  0.  0.] <br>
 [ 0.  0.  0.  1.  0.]] <br>
np.identity: <br>
[[ 1.  0.  0.  0.] <br>
 [ 0.  1.  0.  0.] <br>
 [ 0.  0.  1.  0.] <br>
 [ 0.  0.  0.  1.]] <br>
np.ones and np.zeros: <br>
[[ 1.  1.] <br>
 [ 1.  1.] <br>
 [ 1.  1.]] <br>
[[ 0.  0.  0.] <br>
 [ 0.  0.  0.]] <br>
np.full: <br>
[[ 7.  7.  7.  7.] <br>
 [ 7.  7.  7.  7.] <br>
 [ 7.  7.  7.  7.] <br>
 [ 7.  7.  7.  7.] <br>
 [ 7.  7.  7.  7.]] <br>

#### 乱数による初期化
[乱数](random_value/README.md)のパートで学んだように，乱数による初期化も可能です．
```python
# for Python 2
print np.random.randn(3,2)
print np.random.rand(2,3)
```

```python
# for Python 3
print(np.random.randn(3,2))
print(np.random.rand(2,3))
```
上記のコードを実行すると，下記のように各要素が乱数で初期化されたarrayが表示されます．
[[-0.18099786 -0.70077829]<br>
 [ 0.40147425  1.5462187 ]<br>
 [ 0.45873399  0.93038816]]<br>
[[ 0.10024062  0.63759213  0.02637778]<br>
 [ 0.56373825  0.31275861  0.29290399]]<br>

#### ファイルから読み込んだデータによる初期化
* fromstring(string, dtype=float, count=1, sep=''): 文字列stringから初期化されたデータを1次元arrayとして返す．sepはdelimiter(各データの区切り文字)を表す．
* fromfile(file, dtype=float, count=1, sep=''): fileというファイルから読み込んだデータをarray型オブジェクトとして返す．sepはdelimiter(各データの区切り文字)を表す．
* loadtxt(fname): fnameというファイルから読み込んだデータをarray型オブジェクトとして返す．元のファイルの各行は同じ数のデータを持っている必要がある．

### 要素へのアクセス
numpy.array型オブジェクトの要素へのアクセスは色々な方法があります．
[i,j,...]番目の要素にアクセスするには，x[i,j,...]とします．
部分ベクトル・行列・テンソルへのアクセスも可能です．
詳しくは[このページ](https://docs.scipy.org/doc/numpy/reference/arrays.indexing.html)を参照して下さい．
```python
# for Python 2
x = np.arange(10)
print x
print x[6]
print x[5:]
print x[:5]
print x[1:7:2]
print x[-2:10]
print x[::-1]
```

```python
# for Python 3
x = np.arange(10)
print(x)
print(x[6])
print(x[5:])
print(x[:5])
print(x[1:7:2])
print(x[-2:10])
print(x[::-1])
```
上記のコードを実行すると，下記のように表示されます．<br>
[0 1 2 3 4 5 6 7 8 9]<br>
6<br>
[5 6 7 8 9]<br>
[0 1 2 3 4]<br>
[1 3 5]<br>
[8 9]<br>
[9 8 7 6 5 4 3 2 1 0]<br>

2次元arrayへのアクセス方法は以下のようになります．
```python
# for Python 2
x = np.arange(25).reshape(5,5)
print x
# access an element x(i,j)
i = 1
j = 2
print x[i,j]
# access an i-th row
print x[i,:]
# access an j-th column
print x[:,j]
```

```python
# for Python 3
x = np.arange(25).reshape(5,5)
print(x)
# access an element x(i,j)
i = 1
j = 2
print(x[i,j])
# access an i-th row
print(x[i,:])
# access an j-th column
print(x[:,j])
```
上記のコードを実行すると，下記のように表示されます．<br>
7<br>
[5 6 7 8 9]<br>
[ 2  7 12 17 22]<br>

### 要素の追加
 numpy.array型オブジェクトへの要素の[追加方法](https://docs.scipy.org/doc/numpy/reference/routines.array-manipulation.html)を幾つか紹介する．
 * numpy.concatenate((a1, a2, ...), axis=0): array型オブジェクトa1, a2, ...を連結したarrayを返す．axisは複数のオブジェクトを連結する方向を表す．デフォルト値は0．
 * numpy.column_stack(tup): 複数の1次元もしくは2次元arrayを連結した2次元arrayを返す．
 * numpy.hstack(tup): 複数のarray型オブジェクトを横方向に(column wise)連結したarrayを返す．
 * numpy.vstack(tup): 複数のarray型オブジェクトを縦方向に(row wise)連結したarrayを返す．
```python
# for Python 2
import numpy as np
a = np.full((3,2), 3.0)
b = np.full((3,2), 4.0)

print 'np.concatenate:'
print np.concatenate((a,b), axis=0)
print np.concatenate((a,b), axis=1)

print 'np.column_stack:'
print np.column_stack((a,b))

print 'np.hstack:'
print np.hstack((a,b))

print 'np.vstack:'
print np.vstack((a,b))
```

```python
# for Python 3
import numpy as np
a = np.full((3,2), 3.0)
b = np.full((3,2), 4.0)

print('np.concatenate:')
print(np.concatenate((a,b), axis=0))
print(np.concatenate((a,b), axis=1))

print('np.column_stack:')
print(np.column_stack((a,b)))

print('np.hstack:')
print(np.hstack((a,b)))

print('np.vstack:')
print(np.vstack((a,b)))
```
上記のコードを実行すると，下記のような結果が得られます．<br>
np.concatenate:<br>
[[ 3.  3.] <br>
 [ 3.  3.] <br>
 [ 3.  3.] <br>
 [ 4.  4.] <br>
 [ 4.  4.] <br>
 [ 4.  4.]]<br>
[[ 3.  3.  4.  4.] <br>
 [ 3.  3.  4.  4.] <br>
 [ 3.  3.  4.  4.]]<br>
np.column_stack:<br>
[[ 3.  3.  4.  4.] <br>
 [ 3.  3.  4.  4.] <br>
 [ 3.  3.  4.  4.]]<br>
np.hstack:<br>
[[ 3.  3.  4.  4.] <br>
 [ 3.  3.  4.  4.] <br>
 [ 3.  3.  4.  4.]]<br>
np.vstack: <br>
[[ 3.  3.] <br>
 [ 3.  3.] <br>
 [ 3.  3.] <br>
 [ 4.  4.] <br>
 [ 4.  4.] <br>
 [ 4.  4.]]<br>

### 配列に対する処理
numpyの[*sorting, searching, and counting*](https://docs.scipy.org/doc/numpy-1.11.0/reference/routines.sort.html)にarray型オブジェクトに対する様々な処理が実装された関数の説明が書いてあります．
*numpy.where(condition[, x, y])*<br>
* conditionのみが指定された場合，condition.nonzero()を返す．
* x,yも指定された場合，conditionがTrueであればx，そうでなければyを返す．

*numpy.extract(condition, arr)*<br>
array型オブジェクトarrの各要素の内，conditionを満たす要素を返す．

*numpy.count_nonzero（a)*<br>
array型オブジェクトaの非ゼロ要素の数を返す．

*numpy.nonzero(a)*<br>
入力されたarray型オブジェクトaの非ゼロ要素のインデックスを返す．

*numpy.ndarray.fill(value)*<br>
array型オブジェクトの全要素の値をvalueに設定する．

*random.shuffle(a)*<br>
array型オブジェクトaの要素をランダムにシャッフルする．

*numpy.sort(a, axis=-1, ...)*<br>
入力されたarray型オブジェクトaのソートされたオブジェクトを返す．
axisがNoneの時，ソート前にarrayはflattenされる．

*numpy.argsort(a, axis=-1)*<br>
入力されたarray型オブジェクトaをソートするindexをarray型オブジェクトとして返す．
axisがNoneの時，ソート前にarrayはflattenされる．

*numpy.argmax(a, axis=None, out=None), numpy.argmin(a, axis=None, out=None)*<br>
入力されたarray型オブジェクトaの
* argmax: 最小値
* argmin: 最大値
を持つ要素のインデックスを返す．
