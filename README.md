# Programming-Startup
このレポジトリはC++とPythonでプログラミングを始める人のためのトレーニング用のプログラミング問題集です．
プログラミング問題集と言いつつ，各項目では簡単な説明とコードのサンプル，そして学んだ知識を使った課題を用意してあるだけなので，問題集とは名ばかりの謎の資料です．

このレポジトリにまとめた内容を全てマスターすると，
* 他者が読むことを前提とした，多少は読みやすいコードが書ける
* 自分が書いたプログラムを使った実験を自動で大量に行える
* 大量の実験結果を解析できる(といっても単純に平均値や分散値を計算する程度)
* 実験結果をまとめたグラフもプログラム上で作成できる
ようになれるような内容を心がけて作成していきます．

## テーマ

1. [プログラミング](programming/README.md) (未着手)
* [ターミナル上でのI/O](io_terminal/README.md) (完成)
* [ファイルI/O](io_file/README.md) (完成)
* [パース](misc_parser/README.md) (完成)
* [コメント](misc_comments/README.md) (完成)
* [表明](misc_assertion/README.md) (完成)
* [テスト](misc_test/README.md) (作業中)
* [算術演算](arithmetic/README.md) (完成)
* [乱数](random_value/README.md) (完成)
* [配列](arrays/README.md) (完成)
* [データ解析](analysis/README.md) (完成)
* [可視化](visualization/README.md) (作業中，ファイルI/Oの後に説明，課題追加)
