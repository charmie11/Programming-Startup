# 算術演算 (arithmetic operations)
ここでは算術演算(足し算，引き算，掛け算，割り算)を行う関数の作成を通して，プログラミングの基礎を学びます．

## 課題
無し

## 関数の設計
四則演算(足し算，引き算，掛け算，割り算)は全て2入力，1出力です．

## C++
int型の四則演算を行う関数は以下のように実装します．
似ている処理を実装する時は，引数名を同じにするなど工夫すると，読みやすいコードになります．
```cpp
int add(int a, int b)
{
    return a + b;
}

int sub(int a, int b)
{
    return a - b;
}

int mul(int a, int b)
{
    return a * b;
}

int div(int a, int b)
{
    assert(b != 0 && "divide-by-zero happens here.");
    return a / b;
}

int main()
{
    std::cout << "add(7,4) = " << add(7,4) << std::endl;
    std::cout << "sub(7,4) = " << sub(7,4) << std::endl;
    std::cout << "mul(7,4) = " << mul(7,4) << std::endl;
    std::cout << "div(7,4) = " << div(7,4) << std::endl;
}
```
div関数の最初のassertはゼロによる除算を防ぐための[assert](misc_assertion/README.md)です．

### テンプレート関数
上記のような実装方法だと，異なるデータ型に対する同一の処理を毎回実装する必要が生じてしまいます．
以下のように，引数(の型や数)が異なる同一関数名の関数を定義することを関数のオーバーロードと呼びます．
```cpp
int add(int a, int b)
{
    return a + b;
}

float add(float a, float b)
{
    return a + b;
}

int main()
{
    std::cout << "add(7, 4) = " << add(7, 4) << std::endl;
    std::cout << "add(7.3, 4.2) = " << add(7.3, 4.2) << std::endl;
}
```
関数のオーバーロードによってint型，float型，...用のadd関数が定義出来るわけですが，引数の型以外全く同じ処理をする関数を何個も定義するのは単にめんどくさいというだけではなく，タイプミスなどのヒューマンエラーによるバグの原因になってしまう可能性があります．
そのような問題を解決するための機能がテンプレート関数(template function)です．
template関数によって上記のadd関数を書き直すと以下のようになります．
```cpp
template <typename T>
T add(T a, T b)
{
    return a + b;
}

int main()
{
    std::cout << "add<int>(7, 4) = " << add<int>(7, 4) << std::endl;
    std::cout << "add<float>(7.3, 4.2) = " << add<float>(7.3, 4.2) <<    std::endl;
}
```
template関数の詳しい説明は自身で色々と調べてみてください．

## Python
int型の四則演算を行う関数は以下のように実装します．
似ている処理を実装する時は，引数名を同じにするなど工夫すると，読みやすいコードになります．
```python
# for Python 2
def add(a, b):
    return a + b

def sub(a, b):
    return a - b

def mul(a, b):
    return a * b

def div(a, b):
    assert b != 0, "divide-by-zero happens here."
    return a / b

print "add(7,4) = ", add(7,4)
print "sub(7,4) = ", sub(7,4)
print "mul(7,4) = ", mul(7,4)
print "div(7,4) = ", div(7,4)
```

```python
# for Python 3
def add(a, b):
    return a + b

def sub(a, b):
    return a - b

def mul(a, b):
    return a * b

def div(a, b):
    assert b != 0, "divide-by-zero happens here."
    return a / b

print("add(7,4) =", add(7,4))
print("sub(7,4) =", sub(7,4))
print("mul(7,4) =", mul(7,4))
print("div(7,4) =", div(7,4))
```
div関数の最初のassertはゼロによる除算を防ぐための[assert](misc_assertion/README.md)です．<br>
上記のプログラムを実行すると，以下のような表示がされます．<br>
add(7,4) = 11<br>
sub(7,4) = 3<br>
mul(7,4) = 28<br>
div(7,4) = 1<br>

PythonはC++と異なり関数の定義で入力引数の型指定をしません．
```python
# for Python 2
def add(a, b):
    return a + b

print "add(7,4) =", add(7,4)
print "add(7,4.0) =", add(7,4.0)
```

```python
# for Python 3
def add(a, b):
    return a + b

print("add(7,4) =", add(7,4))
print("add(7,4.0) =", add(7,4.0))
```
上記のプログラムを実行すると，以下のような表示がされます．<br>
add(7,4) = 11<br>
add(7,4.0) = 11.0<br>
