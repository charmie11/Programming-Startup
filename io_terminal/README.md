# ターミナル上でのI/O (I/O on terminal)
ターミナル(コンソール)を介したコンピュータとの対話(インタラクション)，例えばデータの入出力，はプログラミングを行う上で最も一般的な対話方法の一つです．
ここでは，キーボードによる入力，ターミナル(コンソール)への出力方法を学びます．

## 課題
C++, Pythonどちらの言語を使っても構わないので，以下の内容を実装してください．

### 課題1
* 標準出力で氏名を質問し，ユーザがキーボード入力したデータ(スペースを含まない)を標準入力でstring型の変数nameに格納
* 標準出力で年齢を質問し，ユーザがキーボード入力したデータを標準入力でint型の変数ageに格納
* 標準出力で"You are INPUT_NAME and INPUT_AGE years old."と出力(INPUT_NAME, INPUT_AGEには，それぞれnameとageに格納されたデータを表示)
* サンプルファイル: [C++](../code/main_io_terminal1.cpp), [Python](../code/main_io_terminal1.py)

### 課題2
* ユーザがキーボード入力したスペースを複数個含む文字列を標準入力で受け取り，スペースで区切った文字列としてwordsという変数に格納する．
  * C++: wordsはstd::vector< std::string> >型変数とする
  * Python: wordsはlist型変数とする
* 標準出力でwordsの要素数を"Your input data has NUMBER_DATA words."と出力(NUMBER_DATAはwordsの要素数)
* 標準出力でdataの各要素を"words[INDEX] = WORD"と出力(INDEXはwordsの注目要素のインデックス，WORDはINDEX番目のwordsの内容)
* サンプルファイル: [C++](../code/main_io_terminal2.cpp), [Python](../code/main_io_terminal2.py)

## C++
C++における基本的なI/Oについては[このページ](http://www.cplusplus.com/doc/tutorial/basic_io/)に分かりやすくまとめられています．
C++では[iostream](http://cpprefjp.github.io/reference/iostream.html)というヘッダファイルをincludeする必要があります．
C++では，ターミナルやキーボード，ファイルといった時系列的メディア内での入出力を行うためにstreamという概念を導入しています．
streamとは，プログラムが文字列を挿入もしくは抽出する実体です．

iostreamヘッダは以下の関数を提供しています．
sdt::cin, coutのどちらも，C言語のscanf，printf関数と異なり変数の型指定を明示的に行わなくても良いため利便性があります．
以下，順に紹介します(列挙の順序と説明の順序が異なりますが，気にしないでください)．
* cin: 標準入力ストリーム
* cout: 標準出力ストリーム
* clog: 標準ログ(出力)ストリーム
* cerr: 標準エラー(出力)ストリーム

### 標準出力([std::cout](http://cpprefjp.github.io/reference/iostream/cout.html))
std::coutは標準出力のための関数です．
C言語のprint()と異なり，<< オペレータを使って出力データを指定します．
```cpp
#include <iostream>
... (中略) ...
// 文字列の表示
std::cout << "Hello World!!" << std::endl;
// 数値の表示
std::cout << 1234 << std::endl;
// 変数の表示
int x = 100;
std::cout << x << std::endl;
```
上記のコードを実行すると，上から順に <br>
Hello World!! <br>
1234 <br>
100 <br>
とターミナルに表示されます．

複数の異なるデータ，例えば文字列と変数，の内容を同時に出力するためには，**<<** オペレータを複数個使います．
```cpp
#include <iostream>
... (中略) ...
// 色々まとめて表示
std::cout << "Our variable x is set to " << x << " and I am " << 10 " years old." << std::endl;
```
上記のコードを実行すると，<br>
Our variable x is set to 100 and I am 10 years old<br>
とターミナルに表示されます．

### 標準入力([std::cin](http://cpprefjp.github.io/reference/iostream/cin.html))
std::cinは標準入力のための関数です．
先ほど学んだstd::coutとは逆に，**>>** オペレータを使って(一般的には)変数にデータを代入します．
```cpp
#include <iostream>
... (中略) ...
// int型変数への代入
int age;
std::cout << "Please type your age: "
std::cin >> age;
std::cout << age;
// string型変数への代入
std::string name
std::cout << "Please type your name: "
std::cin >> name;
std::cout << name << std::endl;
```
上記のコードを実行すると，まず初めに<br>
Please type your age: <br>
と表示されるので，20と入力すると，<br>
20 <br>
Please type your name: <br>
とターミナルに表示されます．次に，Johnと入力すると，<br>
John<br>
とターミナルに表示されます．

一点注意点があります．
std::cinは空白，タブ，改行を区切り文字とみなします．
上記の例で自分の名前をタイプする時に空白入り文字，例えばJohn Doeと入力しても，変数nameにはJohnしか代入されません．
改行までの1行を一つの文字列として扱いたい場合は，[std::getline](http://cpprefjp.github.io/reference/string/basic_string/getline.html)を使います．

### 標準エラー出力([std::clog](http://cpprefjp.github.io/reference/iostream/clog.html)と[std::cerr](http://cpprefjp.github.io/reference/iostream/cerr.html))
どちらも標準エラーへの出力を行うための関数です．
両者の違いはバッファリングの有無にあります．
std::cerrはバッファリングをしないため直接標準出力へ出力するのにたいし，std::clogはバッファリングをします．


## Python
Pythonの標準入出力を行う関数は標準組み込み関数のため，特にパッケージをimportしなくても使用可能です．

### 標準出力([print](http://docs.python.jp/2/reference/simple_stmts.html#print))
print文は式を逐次的に評価した結果を標準出力に書き出します．
```python
# for Python 2
# 文字列の表示
print "Hello World!!"
# 数値の表示
print 1234
# 変数の表示
x = 100
print x
```

```python
# for Python 3
# 文字列の表示
print("Hello World!!")
# 数値の表示
print(1234)
# 変数の表示
x = 100
print(x)
```
上記のコードを実行すると，上から順に <br>
Hello World!!<br>
1234<br>
100<br>
とターミナルに表示されます．

複数の異なるデータ，例えば文字列と変数，の内容を同時に出力するためには，**,**記号でデータを区切ります．
```python
// 色々まとめて表示
print "Our variable x is set to", x, "and I am", 10, "years old."
```
上記のコードを実行すると，<br>
Our variable x is set to 100 and I am 10 years old<br>
とターミナルに表示されます．
**,**記号は半角スペースに置換される点に気をつけてください．

### 標準入力([raw_input](http://docs.python.jp/2/library/functions.html#raw_input))
raw_input()関数は入力を文字列として受け取ります．
文字列以外の型の変数として受け取る場合は，型指定が必要です．
```python
# for Python 2
# int型変数への代入
print "Please type your age: "
age = int(raw_input())
print age
# string型変数への代入
print "Please type your name: "
name = raw_input()
print name
```

```python
# for Python 3
# int型変数への代入
print("Please type your age: ")
age = int(raw_input())
print(age)
# string型変数への代入
print("Please type your name: ")
name = raw_input()
print(name)
```
上記のコードを実行すると，まず初めに<br>
Please type your age: <br>
と表示されるので，20と入力すると，<br>
20 <br>
Please type your name: <br>
とターミナルに表示されます．次に，Johnと入力すると，<br>
John<br>
とターミナルに表示されます．

raw_input関数の代わりに**[input](http://docs.python.jp/2/library/functions.html#input)**関数を使うと，int, floatの型指定をしなくても，入力に合わせて型を選択してくれます．

#### 複数のデータの入力
1行にスペース区切りで複数のデータが入力される場合は**split**関数を使います．
split関数は文字列をスペース区切りでリストに分割します．
```python
# for Python 2
# 複数文字列への分割
names = raw_input().split()
print names
```

```python
# for Python 3
# 複数文字列への分割
names = raw_input().split()
print(names)
```
上記のコードを実行し，<br>
Alice Ben Christina Diana Emma<br>
と入力すると<br>
['Alice', 'Ben', 'Christina', 'Diana', 'Emma']<br>
とターミナルに表示されます．

上記と同様，複数の整数値データをスペース区切りで入力する場合，map関数を使いsplit()で生成した文字列のlistをint型のlistにマップします．
```python
# for Python 2
# int型のlistへの分割
ages = map(int, raw_input().split())
print ages
```

```python
# for Python 3
# int型のlistへの分割
ages = map(int, raw_input().split())
print(ages)
```
上記のコードを実行し，<br>
1 2 10 20 99<br>
と入力すると<br>
[1, 2, 10, 20, 99]<br>
とターミナルに表示されます．

### 標準エラー出力([sys.stderr](http://docs.python.jp/2/library/sys.html#sys.stderr))
標準エラー出力用の関数はsys.stderrです．
この関数を使用するためには，**sys**パッケージをimportする必要があります．
```python
import sys
... (中略) ...
// エラー出力
sys.stderr.write("Some error happened!!")
```
