# パース （Parse)
ここでは，コマンドラインからプログラムへデータを入力する方法を学びます．
コマンドラインからデータを渡すことで，コードの再コンパイルをすることなくパラメータを変えた実験を大量に行うことができるようになります．

## 課題
C++, Pythonどちらの言語を使っても構わないので，以下の内容を実装してください．

準備
* txtファイルのファイル内容をターミナルに出力するプログラムを作成してください．
* 特定のディレクトリに，内容の異なる複数のtxtファイルを保存してください．

課題
* 上記のファイル一覧を取得し，各ファイルを引数にして上記のプログラムを実行するスクリプトファイルを作成してください．

## C++
main関数に渡す引数をコマンドライン引数(ｃｏｍｍａｎｄ line arguments)と呼びます．
C++のmain関数へ渡せる引数は，以下の2つです．
* 引数の総数
* 引数の文字列を指すポインタの配列

一般的には，main関数の関数宣言を **int main(int argc, char* argv[])** とすると，argcに引数の総数，argvに引数の文字列を指すポインタ配列が代入されます．
以下のコードを実行すると，引数の総数及び書く引数の内容が列挙されます．
```cpp
#include <iostream>
int main(int argc, char* argv[]){
    std::cout << "The number of arguments: " << argc << std::endl;
    for(int n = 0; n < argc; ++n){
        std::cout << "arg[" << n << "]: " << argv[n] << std::endl;
    }

    return 0;
}
```

ターミナルからコマンドライン引数を渡すためには<br>
**実行ファイル名 第1引数 第2引数 ...** <br>
と，スペース区切りで引数を列挙していきます．
実行ファイル名は第0引数として扱われます．

例えば，main_parserという実行ファイルを以下のコマンドで実行すると<br>
  **./main_parser arg1 arg2 arg3** <br>
argc, argvにはそれぞれ以下の値が格納されます．
* argc: 4
* argv[0]: 文字列*./test_parse*の先頭アドレス
* argv[1]: 文字列*arg1*の先頭アドレス
* argv[2]: 文字列*arg2*の先頭アドレス
* argv[3]: 文字列*arg3*の先頭アドレス

## Python
Pythonでコマンドライン引数を取得するためには，sysモジュールの属性であるargvを使用します．
sys.argvはコマンドライン引数を文字列のlistとして返します．
```python
# for Python 2
import sys

# コマンドライン引数のリストを取得
argvs = sys.argv
# コマンドライン引数の総数を取得
argc = len(argvs)
print "The number of arguments:", argc
n = 0
for n, argv in enumerate(argvs):
    print "arg[%d]: %s" % (n, argv)
```

```python
# for Python 3
import sys

# コマンドライン引数のリストを取得
argvs = sys.argv
# コマンドライン引数の総数を取得
argc = len(argvs)
print("The number of arguments:", argc)
n = 0
for n, argv in enumerate(argvs):
    print("arg[%d]: %s" % (n, argv))
```

ターミナルからコマンドライン引数を渡すためには<br>
**python ファイル名 第1引数 第2引数 ...** <br>
と，スペース区切りで引数を列挙していきます．
C++同様，実行ファイル名は第0引数として扱われます．<bt>
SpyderなどのIDEのターミナルから実行する場合は
**runfile('ファイル名',args='第1引数 第2引数 ...')**<br>
と，args=''もしくはargs=""の中にスペース区切りで引数を列挙します．

例えば，main_parser.pyというファイルを以下のコマンドで実行すると<br>
  **python main_parser.py arg1 arg2 arg3** <br>
argc, argvsにはそれぞれ以下の値が格納されます．
* argc: 4
* argvs[0]: 文字列*main_parser.py*
* argvs[1]: 文字列*arg1*
* argvs[2]: 文字列*arg2*
* argvs[3]: 文字列*arg3*

## スクリプトファイルによるバッチ処理
プログラムに対してコマンドライン引数を指定するメリットの1つに，スクリプトと組み合わせたバッチ処理が挙げられます．
Unix/Linuxではシェルスクリプト(.shファイル)を使い，Windowsではバッチファイル(.batファイル)を使います．
ここでは，上記のmain_parserを例に，Windows, Unix/Linuxでのスクリプトファイルの書き方を学びます．
具体的には，以下の内容をC++, Pythonのそれぞれに対してスクリプトファイルの実例を示しながら説明をしていきます．
* 引数の数を変えてmain_parserを複数回実行
* 等差数列的に引数(整数値)を変えてmain_parserを複数回実行
* 特定のディレクトリ内に保存されている特定拡張子のファイル一覧を取得し，各ファイル名を引数としてmain_parserを複数回実行
簡単な例を示すだけなので，基本的な文法や詳細については自身で調べてください．

### Windows (.batファイル)
#### 引数の数を変えてmain_parserを複数回実行
以下の内容をmain_parser.batとして保存し，コマンドラインから実行すると，引数を **arg1** ，**arg1 arg2**， ...と変化させてプログラムを実行します．
```bat
set "CUR_DIR=%~dp0"
# for C++
%CUR_DIR%\build\Debug\main_parser.exe arg1
%CUR_DIR%\build\Debug\main_parser.exe arg1 arg2
%CUR_DIR%\build\Debug\main_parser.exe arg1 arg2 arg3
%CUR_DIR%\build\Debug\main_parser.exe arg1 arg2 arg3 arg4
# for Python
python main_parser.py arg1
python main_parser.py arg1 arg2
python main_parser.py arg1 arg2 arg3
python main_parser.py arg1 arg2 arg3 arg4
```
* 1行目: CUR_DIRという変数を定義し，カレントディレクトリ(~dp0)を文字列として格納
* 3-6行目: 引数を **arg1** ，**arg1 arg2**， ...と変化させC++のコードを実行
* 8-11行目: 引数を **arg1** ，**arg1 arg2**， ...と変化させPythonコードを実行

#### 等差数列的に引数(整数値)を変えてmain_parserを複数回実行
以下の内容をmain_parser_for.batとして保存し，コマンドラインから実行すると，引数を 引数を自動で計算してプログラムを実行します．
```bat
set "CUR_DIR=%~dp0"
# run main_parser with 1, 2, ..., 5
for /L %%i in (1,1,5) do %CUR_DIR%\build\Debug\main_parser.exe %%i
for /L %%i in (1,1,5) do python main_parser.py %%i
# run main_parser with 10, 20, ..., 50
for /L %%i in (10,10,50) do %CUR_DIR%\build\Debug\main_parser.exe %%i
for /L %%i in (10,10,50) do python main_parser.py %%i
```
* 1行目: CUR_DIRという変数を定義し，カレントディレクトリ(~dp0)を文字列として格納
* 3行目: 引数を 1, 2, ..., 5と1から1ずつ足して5まで変化させC++のコードを実行
* 4行目: 引数を 1, 2, ..., 5と1から1ずつ足して5まで変化させPythonのコードを実行
* 6行目: 引数を 10, 20, ..., 50と10から10ずつ足して50まで変化させC++コードを実行
* 7行目: 引数を 10, 20, ..., 50と10から10ずつ足して50まで変化させC++コードを実行

#### 特定のディレクトリ内に保存されている特定拡張子のファイル一覧を取得し，各ファイル名を引数としてmain_parserを複数回実行
```bat
set "CUR_DIR=%~dp0"
# run main_parser with each .cpp file as its argument
for /f "usebackq" %%i in (`dir /B *.cpp`) do %CUR_DIR%\build\Debug\main_parser.exe %%i
for /f "usebackq" %%i in (`dir /B *.cpp`) do python main_parser.py %%i
```
* for /f "usebackq" %%i in (`dir /B *.cpp`): カレントディレクトリでdirコマンドを実行し，拡張子がcppのファイルを1行ずつ変数%%iに格納し，do以下のコマンドを実行
* 1行目: CUR_DIRという変数を定義し，カレントディレクトリ(~dp0)を文字列として格納
* 3行目: カレントディレクトリ内に保存されている各.cppファイルのファイル名を引数としてC++のコードを実行
* 4行目: カレントディレクトリ内に保存されている各.cppファイルのファイル名を引数としてPythonのコードを実行

### Linux (.shファイル)
#### 引数の数を変えてmain_parserを複数回実行
以下の内容をmain_parser.shとして保存し，コマンドラインから実行すると，引数を **arg1** ，**arg1 arg2**， ...と変化させてプログラムを実行します．
```bat
#!/bin/bash
CUR_DIR=`dirname $0`
# for C++
$CUR_DIR/build/main_parser arg1
$CUR_DIR/build/main_parser arg1 arg2
$CUR_DIR/build/main_parser arg1 arg2 arg3
$CUR_DIR/build/main_parser arg1 arg2 arg3 arg4
# for Python
python main_parser.py arg1
python main_parser.py arg1 arg2
python main_parser.py arg1 arg2 arg3
python main_parser.py arg1 arg2 arg3 arg4
```
* 2行目: CUR_DIRという変数を定義し，カレントディレクトリ($0)を文字列として格納
* 4-7行目: 引数を **arg1** ，**arg1 arg2**， ...と変化させC++のコードを実行
* 9-12行目: 引数を **arg1** ，**arg1 arg2**， ...と変化させPythonコードを実行

#### 等差数列的に引数(整数値)を変えてmain_parserを複数回実行
以下の内容をmain_parser_for.batとして保存し，コマンドラインから実行すると，引数を 引数を自動で計算してプログラムを実行します．
```bat
#!/bin/bash
CUR_DIR=`dirname $0`
# for C++
for var in `seq 1 5`
do
	$CUR_DIR/build/main_parser $var
done
var=10
until [ $var -gt 50 ]; do
	$CUR_DIR/build/main_parser $var
	let var+=10
done
# for Python
for var in `seq 1 5`
do
	python main_parser.py $var
done
var=10
until [ $var -gt 50 ]; do
	python main_parser.py $var
	let var+=10
done
```
* 1行目: CUR_DIRという変数を定義し，カレントディレクトリ($0)を文字列として格納
* 4-7行目: 引数を 1, 2, ..., 5と1から1ずつ足して5まで変化させC++のコードを実行
* 8-12行目: 引数を 10, 20, ..., 50と10から10ずつ足して50まで変化させC++コードを実行
* 14-17行目: 引数を 1, 2, ..., 5と1から1ずつ足して5まで変化させPythonのコードを実行
* 18-22行目: 引数を 10, 20, ..., 50と10から10ずつ足して50まで変化させPythonコードを実行

#### 特定のディレクトリ内に保存されている特定拡張子のファイル一覧を取得し，各ファイル名を引数としてmain_parserを複数回実行
```bat
#!/bin/bash
CUR_DIR=`dirname $0`
# for C++
for file in *.cpp;
do
	$CUR_DIR/build/main_parser $file
done
# for Python
for file in *.cpp;
do
	python main_parser.py $file
done
```
* 1行目: CUR_DIRという変数を定義し，カレントディレクトリ(~dp0)を文字列として格納
* 4-7行目: カレントディレクトリ内に保存されている各.cppファイルのファイル名を引数としてC++のコードを実行
* 9-12行目: カレントディレクトリ内に保存されている各.cppファイルのファイル名を引数としてPythonのコードを実行
